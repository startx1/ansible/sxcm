# SXCM : Cluster role

The purpose of this role is to create, start, stop, deploy, profiles, info, delete and destroy an OCP/OKD cluster.
This role is part of the [STARTX sxcm ansible collection](https://galaxy.ansible.com/startxfr/sxcm).

## Requirements

- Ansible runtime
- Installation of the requirements with `ansible-galaxy install -r meta/requirements.yml`
- This depend on the [startxfr.sxcm.workingcopy role](https://startx-ansible-sxcm.readthedocs.io/en/latest/roles/workingcopy/) context. If not already executed into your playbook, this role wil be initialize.

## Role Variables

| Key                                 | Default                | Description                                                                                   |
| ----------------------------------- | ---------------------- | --------------------------------------------------------------------------------------------- |
| sx_cluster_action                   | none                   | The action to perform (could be create, start, stop, deploy, profiles, info, delete, destroy) |
| sx_cluster_config_name              | demox                  | cluster name                                                                                  |
| sx_cluster_config_scope             | dprn                   | scope name                                                                                    |
| sx_cluster_config_environment       | dev                    | environment name                                                                              |
| sx_cluster_config_owner             | startx                 | owner name                                                                                    |
| sx_cluster_config_kind              | managed                | cluster kind (management or managed)                                                          |
| sx_cluster_config_profile           | default                | cluster profile (used during creation)                                                        |
| sx_cluster_config_baseDomain        | ocplab.startx.fr       | cluster base domain (used during creation)                                                    |
| sx_cluster_config_pullSecret        | ''                     | cluster pull secret used for provisionning (used during installation)                         |
| sx_cluster_config_sshKey            | ''                     | cluster ssh key used during provisionning                                                     |
| sx_cluster_opts_config_file         | install-config.yaml    | cluster configuration filename                                                                |
| sx_cluster_opts_deployedmarker_file | .is-deployed           | is-deployed marker filename                                                                   |
| sx_cluster_opts_startedmarker_file  | .is-started            | is-started marker filename                                                                    |
| sx_cluster_install_definition       | ""                     | Variable used to load the content of the install-config.yaml yaml file                        |
| sx_cluster_exist                    | false                  | gitops cluster configuration exist (boolean)                                                  |
| sx_cluster_deployed                 | false                  | gitops cluster is deployed (boolean)                                                          |
| sx_cluster_started                  | false                  | gitops cluster is started (boolean)                                                           |
| sx_cluster_info.suffix              | info-cluster.txt       | Destination suffix for the cluster informations filename                                      |
| sx_cluster_info.dest                | /tmp                   | Destination directory for the cluster informations                                            |
| sx_cluster_profiles_suffix          | cluster-profiles.txt   | Destination suffix for the cluster profiles list                                              |
| sx_cluster_profiles_dest            | /tmp                   | Destination directory for the cluster profiles list                                           |
| sx_cluster_profiles_list            | nano, default          | Profiles list                                                                                 |
| sx_cluster_commit_message           | default commit message | gitops commit message                                                                         |

## Dependencies

- `ansible.builtin` collection
- `startxfr.sxcm` collection. See [startx sxcm collection documentation](https://galaxy.ansible.com/startxfr/sxcm)
- `community.aws` collection. See [aws community collection documentation](https://galaxy.ansible.com/community/aws)

## Example playbooks

### Create cluster playbook

Configure a git repository to store the new cluster definition.

```yaml
- name: Create cluster configuration
  hosts: localhost
  roles:
    - role: startxfr.sxcm.cluster
      vars:
        sx_cluster_action: create
        sx_cluster_config:
          name: mycluster
          scope: myscope
          environment: myenv
          owner: startx
          kind: managed
          profile: default
          baseDomain: mycluster.startx.fr
          pullSecret: { "auths": { "fake": { "auth": "aWQ6cGFzcwo=" } } }
          sshKey: "my_ssh_key"
```

### Deploy cluster playbook

Deploy a previously created environment

```yaml
- name: Deploy cluster
  hosts: localhost
  roles:
    - role: startxfr.sxcm.cluster
      vars:
        sx_cluster_action: deploy
        sx_cluster_config:
          name: mycluster
          scope: myscope
          environment: myenv
          owner: startx
          kind: managed
          profile: default
```

### Info cluster playbook

Get information about a cluster

```yaml
- name: Info cluster
  hosts: localhost
  roles:
    - role: startxfr.sxcm.cluster
      vars:
        sx_cluster_action: info
        sx_cluster_config:
          name: mycluster
          scope: myscope
          environment: myenv
          owner: startx
          kind: managed
          profile: default
```

### Destroy cluster playbook

Destroy cluster service.

```yaml
- name: destroy cluster
  hosts: localhost
  roles:
    - role: startxfr.sxcm.cluster
      vars:
        sx_cluster_action: destroy
        sx_cluster_config:
          name: mycluster
          scope: myscope
          environment: myenv
          owner: startx
          kind: managed
          profile: default
```
