# SXCM : Working-copy role

The purpose of this role is to manipulate git element.
This role is part of the [STARTX services ansible collection](https://galaxy.ansible.com/startxfr/services).

## Requirements

- Ansible runtime
- Installation of the requirements with `ansible-galaxy install -r meta/requirements.yml`
- This depend on the [startxfr.sxcm.repository role](https://startx-ansible-sxcm.readthedocs.io/en/latest/roles/repository/) context. If not already executed into your playbook, this role wil be initialize.

## Role Variables

| Key                  | Variable                                  | Description                                                                                                                    |
| -------------------- | ----------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------ |
| sx_git_action         | [clone,commit,pull,push,delete] | Action to perfom on the git|
| sx_git_remote_url     | git remote url  | repository url to clone from |
| sx_git_remote_name    | git remote name  | remote name to use   |
| sx_git_branch         | git branch name  | branch name to use   |
| sx_git_path           | filesystem path | any filesystem path for cloning destination |
| sx_git_info.mode      | log | not used |
| sx_git_info.suffix    | workingcopy-info.txt | not used |
| sx_git_info.dest      | /tmp  | not used |
| sx_git_list.mode      | log  | not used |
| sx_git_list.suffix    | workingcopy-list.txt | not used |
| sx_git_list.dest      | /tmp | not used |
| sx_git_history.mode   | log   | not used |
| sx_git_history.suffix | workingcopy-history.txt  | not used |
| sx_git_history.dest   | /tmp | not used |
| sx_git_history_limit  | 30  | not used |
| sx_git_commit_message   | Commit message | Message to use when commiting git changes |
| sx_git_init_force     | false | not used |
| sx_git_init_template  | default | not used |
| sx_git_description    | {}     | not used |

## Dependencies

- `ansible.builtin` collection
- `startxfr.sxcm` collection. See [startx sxcm collection documentation](https://galaxy.ansible.com/startxfr/sxcm)

## Example playbooks

### Install workingcopy playbook

Clone a repository from url.

```yaml
- name: Clone git
  hosts: localhost
  roles:
  - role: startxfr.sxcm.git
    sx_git_action: clone
    sx_git_path: /tmp/example
    sx_git_remote_url: git@github.com:example/example.git
```

### Commit changes

Commit change in git

```yaml
- name: Commit change
  hosts: localhost
  roles:
    - role: startxfr.sxcm.git
      sx_git_action: "commit"
      sx_git_commit_message: "initial commit"
      sx_git_path: /tmp/example
```

### Push commit

Delete git (locally).

```yaml
- name: Push commit
  hosts: localhost
  roles:
    - role: startxfr.sxcm.git
      sx_git_path: /tmp/example
      sx_git_action: "push"
```

### Deleting a git

Delete git (locally).

```yaml
- name: Delete git
  hosts: localhost
  roles:
    - role: startxfr.sxcm.git
      sx_git_path: /tmp/example
      sx_git_action: "delete"
```
