# SXCM : infra role

The purpose of this role is to create, start, stop and delete an SXCM infrastructure.
This role is part of the [STARTX sxcm ansible collection](https://galaxy.ansible.com/startxfr/sxcm).

## Requirements

- Ansible runtime
- Installation of the requirements with `ansible-galaxy install -r meta/requirements.yml`

## Role Variables

| Key                                 | Default                | Description                                                                                   |
| ----------------------------------- | ---------------------- | --------------------------------------------------------------------------------------------- |
| sx_infra_action                   | none                   | The action to perform (could be create, start, stop, deploy, profiles, info, delete, destroy) |
| sx_infra_config_name              | demox                  | infra

## Dependencies

- `ansible.builtin` collection
- `startxfr.infra` collection. See [startx infra collection documentation](https://galaxy.ansible.com/startxfr/infra)
- `community.aws` collection. See [aws community collection documentation](https://galaxy.ansible.com/community/aws)

## Example playbooks

### Create infra playbook

Configure a git repository to store the new infra definition.

```yaml
- name: Create infra configuration
  hosts: localhost
  roles:
    - role: startxfr.sxcm.infra
      vars:
        sx_infra_action: create
        sx_infra_config:
          name: myinfra
          scope: myscope
          environment: myenv
          owner: startx
          kind: managed
          profile: default
          baseDomain: myinfra.startx.fr
          pullSecret: { "auths": { "fake": { "auth": "aWQ6cGFzcwo=" } } }
          sshKey: "my_ssh_key"
```

### Deploy infra playbook

Deploy a previously created environment

```yaml
- name: Deploy infra
  hosts: localhost
  roles:
    - role: startxfr.sxcm.infra
      vars:
        sx_infra_action: deploy
        sx_infra_config:
          name: myinfra
          scope: myscope
          environment: myenv
          owner: startx
          kind: managed
          profile: default
```

### Info infra playbook

Get information about a infra

```yaml
- name: Info infra
  hosts: localhost
  roles:
    - role: startxfr.sxcm.infra
      vars:
        sx_infra_action: info
        sx_infra_config:
          name: myinfra
          scope: myscope
          environment: myenv
          owner: startx
          kind: managed
          profile: default
```

### Destroy infra playbook

Destroy infra service.

```yaml
- name: destroy infra
  hosts: localhost
  roles:
    - role: startxfr.sxcm.infra
      vars:
        sx_infra_action: destroy
        sx_infra_config:
          name: myinfra
          scope: myscope
          environment: myenv
          owner: startx
          kind: managed
          profile: default
```
