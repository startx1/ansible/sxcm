# SXCM : Repository role

The purpose of this role is to create and delete a git repository on the selected provider.
This role is part of the [STARTX sxcm ansible collection](https://galaxy.ansible.com/startxfr/sxcm).

## Requirements

- Ansible runtime
- Installation of the requirements with `ansible-galaxy install -r meta/requirements.yml`

## Role Variables

| Key                         | Variable             | Description       |
| --------------------------- | ------------------- | ------------------------------------- |
| sx_repo_action              | [create, delete] (**default: create**) | Action to perfom on the gitops repository |
| sx_repo_provider            | [gitlab, codecommit] | repository provider |
| sx_repo_name                | any git valid name | Name of the repository (mandatory) |
| sx_repo_url                 | valid git url | Url to use for the remote repository. If vault is enabled, will be generated and stored into vault on creation and retrived during init for all other operations. If vault is disabled must be set every times |
| sx_repo_access_mode         | [http, git] | Protocol to use for various actions |
| sx_repo_ssh_key             | ssh private key | Private key to use for repository (stored in vault if enabled) |
| sx_repo_ssh_keypub          | valid ssh public key | Public key to use for repository (stored in vault if enabled) |
| sx_repo_info.mode           | log                 | not used |
| sx_repo_info.suffix         | info-repository.txt | not used |
| sx_repo_info.dest           | /tmp                | not used |
| sx_repo_gitlab_api_url      | any gitlab instance url (**default: https://gitlab.com**) | Gitlab URL for api transaction |
| sx_repo_gitlab_api_token    | gitlab api token | Gitlab token to authenticate for api usage |
| sx_repo_gitlab_userspace    | gitlab username | Gitlab userspace used to create repository  |
| sx_repo_gitlab_keyname      | repository ssh key name | name of the ssh key declared into the gitlab repository |
| sx_repo_gitlab_groupid      | N/A | Group for the repository to be created |
| sx_repo_vault_enable        | boolean (**default:false**)           | Enable storing repository description to vault. use ansible*hashi_vault*\* variables, see [documentation](https://docs.ansible.com/ansible/latest/collections/community/hashi_vault/hashi_vault_lookup.html#ansible-collections-community-hashi-vault-hashi-vault-lookup)|
| sx_repo_vault_path          | any kv path  | Path to the configuration store (kv path may be differ depending on version, see [documentation](https://www.vaultproject.io/docs/secrets/kv)) |
| sx_repo_vault_cache         | {}                  | not used |
| sx_repo_vault_cascadedelete | false               | not used |
| ia_codecommit_*             |                     | if provider is codecommit use [startx infra collection documentation](https://startx-ansible-infra.readthedocs.io/en/latest/roles/codecommit/) configuration  |

## Dependencies

- `ansible.builtin` collection
- `startxfr.infra` collection. See [startx infra collection documentation](https://galaxy.ansible.com/startxfr/infra)
- `community.general` collection. See [community general collection documentation](https://galaxy.ansible.com/community/general)

## Example playbooks

### Create codecommit repository playbook

Create codecommit repository default configuration.

```yaml
- name: Create codecommit repository
  hosts: localhost
  roles:
    - role: startxfr.sxcm.repository
      vars:
        sx_repo_action: "create"
        sx_repo_provider: "codecommit"
        sx_repo_name: "myrepository"
        # ia_codecommit_branch: "master"
        ia_codecommit_aws_accessKey: "AKIAXXXXXXXXXXXXXXXX"
        ia_codecommit_aws_secretKey: "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
        ia_codecommit_aws_region: "eu-west-3"
```

### Create gitlab repository playbook

Create gitlab repository default configuration.

```yaml
- name: Create gitlab repository
  hosts: localhost
  roles:
    - role: startxfr.sxcm.repository
      vars:
        sx_repo_action: "create"
        sx_repo_provider: "gitlab"
        sx_repo_name: "myrepository"
        sx_repo_gitlab_api_url: "https://gitlab.com"
        sx_repo_gitlab_api_token: "XXXXXXXXXXXXXXXXXXXXXXXXXXX"
        sx_repo_gitlab_userspace: "startx-demo"
        sx_repo_gitlab_groupid: "startx-demo"
        sx_repo_gitlab_keyname: "generated-key"
        sx_repo_vault_enable: false
```

### Info repository playbook

Get information on default repository.

```yaml
- name: Information repository
  hosts: localhost
  roles:
    - role: startxfr.sxcm.repository
      vars:
        sx_repo_action: "info"
```

### Delete repository playbook

Delete repository default configuration.

```yaml
- name: Delete repository
  hosts: localhost
  roles:
    - role: startxfr.sxcm.repository
      vars:
        sx_repo_action: "delete"
        sx_repo_provider: "codecommit"
        sx_repo_name: "myrepository"
        ia_codecommit_aws_accessKey: "AKIAXXXXXXXXXXXXXXXX"
        ia_codecommit_aws_secretKey: "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
        ia_codecommit_aws_region: "eu-west-3"
```
