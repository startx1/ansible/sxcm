# SXCM : World role

The purpose of this role is to create, info and delete a git sxcm world.
This role is part of the [STARTX sxcm ansible collection](https://galaxy.ansible.com/startxfr/sxcm).

## Requirements

- Ansible runtime
- Installation of the requirements with `ansible-galaxy install -r meta/requirements.yml`

## Role Variables

| Key                         | Default             | Description                                                                                                                                                                                                    |
| --------------------------- | ------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| sx_world_action              | create              | Action to perfom on the gitops world                                                                                                                                                                      |

## Dependencies

- `ansible.builtin` collection
- `awx.awx` collection. See [AWX collection documentation](https://galaxy.ansible.com/awx/awx)

## Example playbooks

### Create World playbook

Create World default configuration.

```yaml
- name: Create world
  hosts: localhost
  roles:
    - role: startxfr.sxcm.world
      vars:
        sx_world_action: "create"
        sx_world_name: "myworld"
```

### Info world playbook

Get information on default world.

```yaml
- name: Information world
  hosts: localhost
  roles:
    - role: startxfr.sxcm.world
      vars:
        sx_world_action: "info"
        sx_world_name: "myworld"
```

### Delete world playbook

Delete world default configuration.

```yaml
- name: Delete world
  hosts: localhost
  roles:
    - role: startxfr.sxcm.world
      vars:
        sx_world_action: "delete"
        sx_world_name: "myworld"
```
