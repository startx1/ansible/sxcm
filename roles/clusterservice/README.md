# StartXClusterManager : Clusterservice role

Read the [startxfr.sxcm.clusterservice role documentation](https://startx-ansible-sxcm.readthedocs.io/en/latest/roles/clusterservice/)
for more information on how to use [STARTX sxcm ansible collection](https://galaxy.ansible.com/startxfr/sxcm) clusterservice role.
