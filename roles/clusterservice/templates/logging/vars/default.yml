vars:
  # Default values for configuration of a STARTX cluster
  # see values.yaml for explanation on each params
  context: &context
    scope: "{[% lookup('vars', 'sx_cluster_config_scope') | default('myscope') %]}"
    cluster: "{[% lookup('vars', 'sx_cluster_config_name') | default('mycluster') %]}"
    environment: "{[% lookup('vars', 'sx_cluster_config_environment') | default('myenv') %]}"
    version: "{[% lookup('vars', 'sx_cluster_config_version') | default('0.0.1') %]}"
    component: logging
    app: startx-logging
  logging:
    enabled: false
    infra_enabled: false
    hooked: false
    managementState: "Managed"
    expose:
      enabled: false
      destinationCACert: |
        -----BEGIN CERTIFICATE-----
        place here your elasticsearch certificate. 
        You can get it with 
        oc extract secret/elasticsearch \
        -n openshift-logging --to=. --keys=admin-ca && \
        cat admin-ca
        -----END CERTIFICATE-----
    elasticsearch:
      enabled: true
      replicas: 3
      resources:
        limits:
          memory: 16Gi
        requests:
          cpu: 500m
          memory: 16Gi
      storage:
        class: "gp2"
        size: "200G"
    kibana:
      enabled: true
      replicas: 1
      resources:
        limits:
          memory: 736Mi
        requests:
          cpu: 100m
          memory: 736Mi
    curation:
      enabled: true
      schedule: "30 3 * * *"
      resources:
        limits:
          memory: 256Mi
        requests:
          cpu: 100m
          memory: 256Mi
    fluentd:
      enabled: true
      resources:
        limits:
          memory: 736Mi
        requests:
          cpu: 100m
          memory: 736Mi
  eventrouter:
    enabled: false
    replicas: 1
    resources:
      limits:
        cpu: 400m
        memory: 512Mi
      requests:
        cpu: "100m"
        memory: 128Mi
  logforwarder:
    enabled: false
    spec: |
      pipelines: 
      - name: all-to-default
        inputRefs:
        - infrastructure
        - application
        - audit
        outputRefs:
        - default

  # Configuration of the project (see https://startxfr.github.io/helm-repository/charts/project)
  project: 
    enabled: false
    context: 
      <<: *context
    project: 
      enabled: true
      type: namespace
      name: "openshift-logging"
      display_name: "Startx LOGGING"
      requester: startx
      description: Openshift logging configured by STARTX
    rbac: 
      enabled: true
      groups: 
      - id: devops-view
        name: devops
        role: view
      - id: ops-admin
        name: ops
        role: admin
    networkpolicy: 
      enabled: false
      rules: 
      - id: allow-openshift-operators-redhat
        spec: |
          podSelector: null
          ingress:
          - from:
            - namespaceSelector:
                matchLabels:
                  project: openshift-operators-redhat
    limits: 
      enabled: false
    quotas: 
      enabled: false
      
  # Configuration of the operator (see https://startxfr.github.io/helm-repository/charts/operator)
  operator:
    enabled: false
    context: 
      <<: *context
    subscription:
      enabled: true
      name: "cluster-logging"
      namespace: "openshift-logging"
      version: "5.4.0-138"
      operator: 
        channel: "stable-5.4"
        name: cluster-logging
        installPlanApproval: Automatic
        # csv: cluster-logging
        source: 
          name: redhat-operators
          namespace: openshift-marketplace
    operatorGroup:
      enabled: true
      hooked: false
      name: "openshift-logging"
      namespace: "openshift-logging"
      providedAPIs: "ClusterLogForwarder.v1.logging.openshift.io,ClusterLogging.v1.logging.openshift.io"


  # Configuration of the elasticSearch operator (see https://startxfr.github.io/helm-repository/charts/operator)
  operatorElastic:
    enabled: false
    context: 
      <<: *context
    subscription:
      enabled: true
      hooked: false
      name: "elasticsearch-operator"
      namespace: "openshift-operators-redhat"
      version: "5.4.0-152"
      operator: 
        channel: "stable-5.4"
        name: elasticsearch-operator
        installPlanApproval: Automatic
        # csv: elasticsearch-operator
        source: 
          name: redhat-operators
          namespace: openshift-marketplace
    operatorGroup:
      enabled: true
      hooked: false
      target: "all-ns"
      name: "elasticsearch-operator"
      namespace: "openshift-operators-redhat"
      providedAPIs: "Elasticsearch.v1.logging.openshift.io,Kibana.v1.logging.openshift.io"